package org.wchua.timetabling.domain.fact;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.DayOfWeek;
import java.time.LocalTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TimeSlot {
    DayOfWeek dayOfWeek;
    LocalTime startTime;
    LocalTime endTime;
}
