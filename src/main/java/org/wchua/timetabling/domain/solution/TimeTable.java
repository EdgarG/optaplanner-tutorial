package org.wchua.timetabling.domain.solution;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.optaplanner.core.api.domain.solution.PlanningEntityCollectionProperty;
import org.optaplanner.core.api.domain.solution.PlanningScore;
import org.optaplanner.core.api.domain.solution.PlanningSolution;
import org.optaplanner.core.api.domain.solution.ProblemFactCollectionProperty;
import org.optaplanner.core.api.domain.valuerange.ValueRangeProvider;
import org.optaplanner.core.api.score.buildin.hardsoft.HardSoftScore;
import org.wchua.timetabling.domain.fact.Room;
import org.wchua.timetabling.domain.fact.TimeSlot;
import org.wchua.timetabling.domain.planning_entity.Lesson;

import java.util.List;

@PlanningSolution
@NoArgsConstructor
@Getter
public class TimeTable {

    @ValueRangeProvider(id = "timeslotRange")
    @ProblemFactCollectionProperty
    private List<TimeSlot> timeSlots;

    @ValueRangeProvider(id = "roomRange")
    @ProblemFactCollectionProperty
    private List<Room> rooms;

    @PlanningEntityCollectionProperty
    private List<Lesson> lessons;

    public TimeTable(List<TimeSlot> timeSlots, List<Room> rooms, List<Lesson> lessons) {
        this.timeSlots = timeSlots;
        this.rooms = rooms;
        this.lessons = lessons;
    }

    @PlanningScore
    private HardSoftScore score;


}
