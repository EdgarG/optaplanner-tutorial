package org.wchua.timetabling.rest;

import org.optaplanner.core.api.solver.SolverJob;
import org.optaplanner.core.api.solver.SolverManager;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.wchua.timetabling.domain.solution.TimeTable;

import java.util.UUID;
import java.util.concurrent.ExecutionException;

@RestController
@RequestMapping("/timetable")
public class TimeTableController {

    private final SolverManager<TimeTable, UUID> solverManager;

    public TimeTableController(SolverManager<TimeTable, UUID> solverManager) {
        this.solverManager = solverManager;
    }


    @PostMapping("/solve")
    public TimeTable solveTimeTable(@RequestBody TimeTable problem) {
        UUID problemId = UUID.randomUUID();
        // Submit the problem to start solving
        SolverJob<TimeTable, UUID> solverJob = solverManager.solve(problemId, problem);
        try {
            // Wait until the solving ends
            return solverJob.getFinalBestSolution();
        } catch (InterruptedException | ExecutionException e) {
            throw new IllegalStateException("Solving failed.", e);
        }
    }
}
