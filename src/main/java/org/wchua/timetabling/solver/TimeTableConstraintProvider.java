package org.wchua.timetabling.solver;

import org.optaplanner.core.api.score.buildin.hardsoft.HardSoftScore;
import org.optaplanner.core.api.score.stream.Constraint;
import org.optaplanner.core.api.score.stream.ConstraintFactory;
import org.optaplanner.core.api.score.stream.ConstraintProvider;
import org.optaplanner.core.api.score.stream.Joiners;
import org.wchua.timetabling.domain.planning_entity.Lesson;

import java.util.ArrayList;
import java.util.List;

/**
 * The ConstraintProvider scales an order of magnitude better than the EasyScoreCalculator: O(n) instead of O(n²).
 */
public class TimeTableConstraintProvider implements ConstraintProvider {
    @Override
    public Constraint[] defineConstraints(ConstraintFactory constraintFactory) {
        List<Constraint> constraints = new ArrayList<>();
        constraints.addAll(hardConstraints(constraintFactory));
        constraints.addAll(softConstraints(constraintFactory));
        return constraints.toArray(new Constraint[0]);
    }

    private List<Constraint> hardConstraints(ConstraintFactory constraintFactory) {
        return List.of(
                roomCanAccommodateAtMostOneLessonAtTheSameTime(constraintFactory),
                teacherCanTeachAtMostOneLessonAtTheSameTime(constraintFactory),
                studentCanAttendAtMostOneLessonAtTheSameTime(constraintFactory)
        );
    }

    private List<Constraint> softConstraints(ConstraintFactory constraintFactory) {
        return List.of();
    }

    protected Constraint roomCanAccommodateAtMostOneLessonAtTheSameTime(ConstraintFactory constraintFactory) {
        String constraintName = "Room can accommodate at most one lesson at the same time";
        var inTheSameTimeslot = Joiners.equal(Lesson::getTimeSlot);
        var inTheSameRoom = Joiners.equal(Lesson::getRoom);
        var uniquePair = Joiners.lessThan(Lesson::getId); // different id, no reverse pairs
        return constraintFactory
                .forEach(Lesson.class)
                // pair lessons
                .join(Lesson.class, inTheSameTimeslot, inTheSameRoom, uniquePair)
                .penalize(constraintName, HardSoftScore.ONE_HARD);
    }

    protected Constraint teacherCanTeachAtMostOneLessonAtTheSameTime(ConstraintFactory constraintFactory) {
        String constraintName = "Teacher can teach at most one lesson at the same time";
        var inTheSameTimeslot = Joiners.equal(Lesson::getTimeSlot);
        var isTheSameTeacher = Joiners.equal(Lesson::getTeacher);
        var uniquePair = Joiners.lessThan(Lesson::getId); // different id, no reverse pairs
        return constraintFactory
                .forEach(Lesson.class)
                // pair lessons
                .join(Lesson.class, inTheSameTimeslot, isTheSameTeacher, uniquePair)
                .penalize(constraintName, HardSoftScore.ONE_HARD);
    }

    protected Constraint studentCanAttendAtMostOneLessonAtTheSameTime(ConstraintFactory constraintFactory) {
        String constraintName = "Student can attend at most one lesson at the same time";
        var inTheSameTimeslot = Joiners.equal(Lesson::getTimeSlot);
        var isTheSameStudentGroup = Joiners.equal(Lesson::getTeacher);
        var uniquePair = Joiners.lessThan(Lesson::getId); // different id, no reverse pairs
        return constraintFactory
                .forEach(Lesson.class)
                // pair lessons
                .join(Lesson.class, inTheSameTimeslot, isTheSameStudentGroup, uniquePair)
                .penalize(constraintName, HardSoftScore.ONE_HARD);
    }

}
