package org.wchua.timetabling.rest;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;
import org.optaplanner.core.api.solver.SolverManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.wchua.timetabling.domain.fact.Room;
import org.wchua.timetabling.domain.fact.TimeSlot;
import org.wchua.timetabling.domain.planning_entity.Lesson;
import org.wchua.timetabling.domain.solution.TimeTable;

import java.time.DayOfWeek;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(properties = {
        // Effectively disable spent-time termination in favor of the best-score-limit
        "optaplanner.solver.termination-limit=1h",
        "optaplanner.solver.termination.best-score-limit=0hard/*soft"
})
class TimeTableControllerTest {

    @Autowired
    private TimeTableController timeTableController;


    @Test
    @Timeout(600_000)
    public void solve() {
        TimeTable problem = generateProblem();
        TimeTable solution = timeTableController.solveTimeTable(problem);
        assertFalse(solution.getLessons().isEmpty());

        for (Lesson lesson : solution.getLessons()) {
            assertNotNull(lesson.getTimeSlot());
            assertNotNull(lesson.getRoom());
        }

        assertTrue(solution.getScore().isFeasible());
    }

    private TimeTable generateProblem() {
        List<TimeSlot> timeslots = new ArrayList<>();
        timeslots.add(new TimeSlot(DayOfWeek.MONDAY, LocalTime.of(8, 30), LocalTime.of(9, 30)));
        timeslots.add(new TimeSlot(DayOfWeek.MONDAY, LocalTime.of(9, 30), LocalTime.of(10, 30)));
        timeslots.add(new TimeSlot(DayOfWeek.MONDAY, LocalTime.of(10, 30), LocalTime.of(11, 30)));
        timeslots.add(new TimeSlot(DayOfWeek.MONDAY, LocalTime.of(13, 30), LocalTime.of(14, 30)));
        timeslots.add(new TimeSlot(DayOfWeek.MONDAY, LocalTime.of(14, 30), LocalTime.of(15, 30)));

        List<Room> rooms = new ArrayList<>();
        rooms.add(new Room("Room A"));
        rooms.add(new Room("Room B"));
        rooms.add(new Room("Room C"));

        List<Lesson> lessons = new ArrayList<>();
        lessons.add(new Lesson(101L, "Math", "B. May", "9th grade"));
        lessons.add(new Lesson(102L, "Physics", "M. Curie", "9th grade"));
        lessons.add(new Lesson(103L, "Geography", "M. Polo", "9th grade"));
        lessons.add(new Lesson(104L, "English", "I. Jones", "9th grade"));
        lessons.add(new Lesson(105L, "Spanish", "P. Cruz", "9th grade"));

        lessons.add(new Lesson(201L, "Math", "B. May", "10th grade"));
        lessons.add(new Lesson(202L, "Chemistry", "M. Curie", "10th grade"));
        lessons.add(new Lesson(203L, "History", "I. Jones", "10th grade"));
        lessons.add(new Lesson(204L, "English", "P. Cruz", "10th grade"));
        lessons.add(new Lesson(205L, "French", "M. Curie", "10th grade"));
        return new TimeTable(timeslots, rooms, lessons);
    }

}