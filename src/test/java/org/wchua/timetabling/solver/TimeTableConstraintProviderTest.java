package org.wchua.timetabling.solver;

import org.junit.jupiter.api.Test;
import org.optaplanner.test.api.score.stream.ConstraintVerifier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.wchua.timetabling.domain.fact.Room;
import org.wchua.timetabling.domain.fact.TimeSlot;
import org.wchua.timetabling.domain.planning_entity.Lesson;
import org.wchua.timetabling.domain.solution.TimeTable;

import java.time.DayOfWeek;
import java.time.LocalTime;

@SpringBootTest
class TimeTableConstraintProviderTest {

    private static final Room ROOM = new Room("Room1");
    private static final TimeSlot TIMESLOT1 = new TimeSlot(DayOfWeek.MONDAY, LocalTime.of(9, 0), LocalTime.NOON);
    private static final TimeSlot TIMESLOT2 = new TimeSlot(DayOfWeek.TUESDAY, LocalTime.of(9, 0), LocalTime.NOON);

    @Autowired
    ConstraintVerifier<TimeTableConstraintProvider, TimeTable> constraintVerifier;

    @Test
    void roomCanAccommodateAtMostOneLessonAtTheSameTime() {
        Lesson firstLesson = new Lesson(1L, "Subject1", "Teacher1", "Group1");
        Lesson conflictingLesson = new Lesson(2L, "Subject2", "Teacher2", "Group2");
        Lesson nonConflictingLesson = new Lesson(3L, "Subject3", "Teacher3", "Group3");

        firstLesson.setRoom(ROOM);
        firstLesson.setTimeSlot(TIMESLOT1);

        conflictingLesson.setRoom(ROOM);
        conflictingLesson.setTimeSlot(TIMESLOT1);

        nonConflictingLesson.setRoom(ROOM);
        nonConflictingLesson.setTimeSlot(TIMESLOT2);

        constraintVerifier.verifyThat(TimeTableConstraintProvider::roomCanAccommodateAtMostOneLessonAtTheSameTime)
                .given(firstLesson, conflictingLesson, nonConflictingLesson)
                .penalizesBy(1);
    }

    @Test
    void teacherCanTeachAtMostOneLessonAtTheSameTime() {
        Lesson firstTeacher = new Lesson(1L, "Subject1", "Teacher1", "Group1");
        Lesson conflictingTeacher = new Lesson(2L, "Subject2", "Teacher1", "Group2");
        Lesson nonConflictingTeacher = new Lesson(3L, "Subject3", "Teacher3", "Group3");

        firstTeacher.setRoom(ROOM);
        firstTeacher.setTimeSlot(TIMESLOT1);

        conflictingTeacher.setRoom(ROOM);
        conflictingTeacher.setTimeSlot(TIMESLOT1);

        nonConflictingTeacher.setRoom(ROOM);
        nonConflictingTeacher.setTimeSlot(TIMESLOT2);

        constraintVerifier.verifyThat(TimeTableConstraintProvider::teacherCanTeachAtMostOneLessonAtTheSameTime)
                .given(firstTeacher, conflictingTeacher, nonConflictingTeacher)
                .penalizesBy(1);
    }

    @Test
    void studentCanAttendAtMostOneLessonAtTheSameTime() {

    }

}